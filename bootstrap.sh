sudo salt-cloud -c . -l debug -p master salt
gcloud compute copy-files * salt: --zone "us-central1-a"
gcloud compute ssh salt --zone "us-central1-a" --command "sh setup.sh"
