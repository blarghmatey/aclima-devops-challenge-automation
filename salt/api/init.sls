deploy_user:
  user.present:
    - name: deploy
    - home: /home/deploy
    - shell: /bin/bash

install_pkgs:
  pkg.installed:
    - names:
        - uwsgi
        - python-pip
        - uwsgi-plugin-python3
        - python3
        - python3-pip
        - python3-dev
        - build-essential
    - refresh: True

app_source:
  git.latest:
    - name: https://gitlab.com/renaissancedev/aclima-devops-challenge-api.git
    - user: deploy
    - target: /home/deploy/helloworld

install_python_deps:
  pip.installed:
    - requirements: /home/deploy/helloworld/requirements.txt
    - bin_env: /usr/bin/pip3

uwsgi_config:
  file.rename:
    - name: /etc/uwsgi/apps-available/helloworld.ini
    - source: /home/deploy/helloworld/uwsgi.ini
    - makedirs: True
    - force: True

activate_uwsgi_config:
  file.symlink:
    - name: /etc/uwsgi/apps-enabled/helloworld.ini
    - target: /etc/uwsgi/apps-available/helloworld.ini

uwsgi_systemd:
  file.managed:
    - name: /lib/systemd/system/uwsgi.service
    - source: salt://api/files/uwsgi.service

uwsgi_service:
  service.running:
    - name: uwsgi
    - enable: True
    - watch:
        - file: uwsgi_config